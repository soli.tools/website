====================================
digital infrastructures for the good
====================================

:save_as: index.html
:url: /
:description:

  soli.tools provides digital infrastructures
  for those who support the good

about
=====

`soli.tools <https://soli.tools>`__ provides digital infrastructures
for those who support the good.
"The good" can be, for example, justice, equality, pluralism, sustainability.
"Support" can be, for example, political, social, educational, cultural,
environmental, scientific, or economic.

services
========

- `lists.soli.tools <https://lists.soli.tools>`__
  – email lists (Mailman)

access
======

If you want access, describe your intended use in an `email
<contact_>`_.

suggestions
===========

Feel also free to suggest tools that you miss on the Web via `email
<contact_>`_.

donations
=========

For infrastructures, real money needs to be spent.
For setup and maintenance, real work hours need to be spent.
See `this spreadsheet
<https://cryptpad.fr/sheet/#/2/sheet/view/rRxinsqmyPq32hcsD-Cm4iG8rA6U05OKNkvzdyGRDbw>`__ for actual costs, approximated costs, and received
donations.

Donations are currently possible via
`Liberapay <https://liberapay.com/lpirl>`__ and
`PayPal <https://www.paypal.com/paypalme/lpirl>`__.
Unless otherwise desired, donated amounts are anonymously listed in the
spreadsheet mentioned above. In case of questions and suggestions,
contact_ me.

contact
=======

Feel invited to send an email to admin at lists.soli.tools.
